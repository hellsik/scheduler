﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Scheduler.API.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Schedule",
                nullable: false,
                defaultValue: new DateTime(2016, 7, 18, 16, 52, 27, 360, DateTimeKind.Local));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "Schedule",
                nullable: false,
                defaultValue: new DateTime(2016, 7, 18, 16, 52, 27, 351, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Schedule",
                nullable: false,
                defaultValue: new DateTime(2016, 7, 18, 16, 35, 16, 809, DateTimeKind.Local));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreated",
                table: "Schedule",
                nullable: false,
                defaultValue: new DateTime(2016, 7, 18, 16, 35, 16, 777, DateTimeKind.Local));
        }
    }
}
